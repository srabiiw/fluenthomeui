import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { UserType } from '../../_models';
import { UserTypeService } from '../../_services';

@Component({
  selector: 'app-list-user-type',
  templateUrl: './list-user-type.component.html',
  styleUrls: ['./list-user-type.component.scss']
})
export class ListUserTypeComponent implements OnInit {

  userTypes: UserType[];

  constructor(private router: Router, private userTypeService: UserTypeService) { }

  ngOnInit() {
    this.userTypeService.getUserTypes()
      .subscribe( data => {
        this.userTypes = data;
      });
  }

  deleteUserType(userType: UserType): void {
    this.userTypeService.deleteUserType(userType.id)
      .subscribe( data => {
        this.userTypes = this.userTypes.filter(u => u !== userType);
      })
  };

  editUserType(userType: UserType): void {
    localStorage.removeItem("editUserTypeId");
    localStorage.setItem("editUserTypeId", userType.id.toString());
    this.router.navigate(['edit-user-type']);
  };

  addUserType(): void {
    this.router.navigate(['add-user-type']);
  };
}