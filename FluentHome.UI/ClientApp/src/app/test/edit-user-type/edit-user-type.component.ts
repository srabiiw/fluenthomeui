import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";
import { UserTypeService } from '../../_services';
import { UserType } from '../../_models';

@Component({
  selector: 'app-edit-user-type',
  templateUrl: './edit-user-type.component.html',
  styleUrls: ['./edit-user-type.component.scss']
})
export class EditUserTypeComponent implements OnInit {

  userType: UserType;
  editForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private userTypeService: UserTypeService) { }

  ngOnInit() {
    let userTypeId = localStorage.getItem("editUserTypeId");

    if(!userTypeId) {
      alert("Invalid action.")
      this.router.navigate(['list-user-type']);
      return;
    }

    this.editForm = this.formBuilder.group({
      id: [],
      userTypeName: ['', Validators.required],
      createdDate: [],
      modifiedDate: [],
      isActive: ['', Validators.required]
    });

    this.userTypeService.getUserTypeById(+userTypeId)
      .subscribe( data => {
        this.editForm.setValue(data);
      });
  }

  onUpdateUserType() {
    this.userTypeService.updateUserType(this.editForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['list-user-type']);
        },
        error => {
          alert(error);
        });
  }

}
