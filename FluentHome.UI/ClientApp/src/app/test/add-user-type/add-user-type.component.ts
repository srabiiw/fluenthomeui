import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";
import { UserTypeService } from '../../_services';
import { UserType } from '../../_models';

@Component({
  selector: 'app-add-user-type',
  templateUrl: './add-user-type.component.html',
  styleUrls: ['./add-user-type.component.scss']
})
export class AddUserTypeComponent implements OnInit {

  userType: UserType;
  addForm: FormGroup;
  constructor(private formBuilder: FormBuilder,private router: Router, private userTypeService: UserTypeService) { }

  ngOnInit() {

    this.addForm = this.formBuilder.group({
      id: [],
      userTypeName: ['', Validators.required],
      createdDate: [],
      modifiedDate: [],
      isActive: ['', Validators.required]
    });

  }

  onSubmitUserType() {
    this.userTypeService.createUserType(this.addForm.value)
      .subscribe( data => {
        this.router.navigate(['list-user-type']);
      });
  }

}