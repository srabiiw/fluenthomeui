import { Component, OnInit } from '@angular/core';
import { ArticleCategory } from '../../_models/articlecategory';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../_services';
import { ArticleService } from '../../_services/article.service';
import { ArticleCategoryService } from '../../_services/article-category.service';
import { Article } from '../../_models/article';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.scss']
})
export class EditArticleComponent implements OnInit {

  editArticleForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private articleService: ArticleService,
    private articleCategoryService: ArticleCategoryService
  ) { }


  imgUrl: string = "/assets/images/default-thumb.jpg";
  fileToUpload: File = null;

  //bookImageUpload
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    //show preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imgUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  /********************************* */
  public articleCategoryId: string = '';
  public articleTitle: string = '';
  public articleDescription: string = '';
  public featuredImage: string = '';
  public isActive: boolean = false;
  articleCategories: ArticleCategory[] = [];
  public editorConfig: any;
  /********************************* */

  ngOnInit() {

    let articleId = localStorage.getItem("editArticleId");
    
    if(!articleId) {
      alert("Invalid action.")
      this.router.navigate(['/article/list']);
      return;
    }

    this.editArticleForm = this.formBuilder.group({
      id: [],
      articleCategoryId: this.articleCategoryId,
      articleTitle: ['', Validators.required],
      articleDescription: this.articleDescription,
      featuredImage: this.featuredImage,
      isActive: this.isActive
    });

    this.articleCategoryService.getArticleCategories().subscribe((data: any) => {
      this.articleCategories = data;
    });

    this.editorConfig = {
      "editable": true,
      "spellcheck": true,
      "height": "auto",
      "minHeight": "0",
      "width": "auto",
      "minWidth": "0",
      "translate": "yes",
      "enableToolbar": true,
      "showToolbar": true,
      // "placeholder": "Enter text here...",
      // "imageEndPoint": "",
      // "toolbar": [
      //   ["bold", "italic", "underline", "strikeThrough"],
      //   ["justifyLeft", "justifyRight", "justifyFull", "outdent"]
      // ]
    };

    this.articleService.getArticleById(+articleId)
      .subscribe( data => {
        this.editArticleForm.setValue(data);
      });
  }

  // convenience getter for easy access to form fields
  get f() { return this.editArticleForm.controls; }

  article: Article;
  onArticleSubmit(formArticle) {

    this.submitted = true;

    // stop here if form is invalid
    if (this.editArticleForm.invalid) {
      return;
    }

    if (this.editArticleForm.valid) {

      this.loading = true;

      this.articleService.addArticle(formArticle)
        .subscribe(
          (data: any) => {
            //this.router.navigate([this.returnUrl]);
            this.router.navigate(['/article/list']);
            this.toastr.success('Article successfully added!');
          },
          error => {
            this.alertService.error(error);
            this.toastr.error('Unable to update article!');
            this.loading = false;
          });

    }

  }

}
