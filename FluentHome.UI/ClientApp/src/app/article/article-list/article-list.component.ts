import { AfterViewInit, Component, OnInit, Renderer } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService, ArticleService } from '../../_services';
import { Article, ArticleList } from '../../_models';
import { HttpClient } from '@angular/common/http';

/****************************************** */

class DataTablesResponse {
  data: any[];
  draw: number;
  recordsFiltered: number;
  recordsTotal: number;
}
/****************************************** */

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements AfterViewInit, OnInit {

  dtOptions: DataTables.Settings = {};
  articleList: ArticleList[];

  public articles: any;

  constructor(
    private renderer: Renderer,
    private http: HttpClient,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private articleService: ArticleService
  ) { }

  ngAfterViewInit(): void {
    this.renderer.listenGlobal('document', 'click', (event) => {
      if (event.target.hasAttribute("view-person-id")) {
        this.router.navigate(["/person/" + event.target.getAttribute("view-person-id")]);
      }
    });
  }

  ngOnInit() {
    this.articleService.getArticles().subscribe((data: any) => {
      this.articles = data;
    });

    const that = this;

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10,
      // lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
      serverSide: true,
      processing: true,
      ajax: (dataTablesParameters: any, callback) => {

        // that.http
        // .post<DataTablesResponse>(
        //   'https://angular-datatables-demo-server.herokuapp.com/',
        //   dataTablesParameters, {}
        // )
        that.articleService.getArticlesFromDataTable(dataTablesParameters)
          .subscribe(resp => {
            that.articleList = resp.data;

            callback({
              recordsTotal: resp.recordsTotal,
              recordsFiltered: resp.recordsFiltered,
              data: []
            });
          });
      },
      // columns: [{
      //   title: 'ID',
      //   data: 'id'
      // }, {
      //   title: 'Article Title',
      //   data: 'articleTitle'
      // }, {
      //   title: 'Article Category',
      //   data: 'articleCategoryName'
      // }, {
      //   title: 'Featured Image',
      //   data: 'featuredImage'
      // }, {
      //   title: 'Is Verified',
      //   data: 'isVerified'
      // }, {
      //   title: 'Action',
      //   render: function (data: any, type: any, full: any) {
      //     return 'View';
      //   }
      // },
      columns: [
        { data: 'id' },
        { data: 'articleTitle' },
        { data: 'articleCategoryName' },
        { data: 'featuredImage' },
        { data: 'isVerified' },
        { data: 'id' },
        {
          title: 'Delete',
          render: function (data: any, type: any, full: any) {
            return 'View';
          }
        },
        { data: 'isActive' }
      ],
      responsive: true
    };
  }

  deleteArticle(article: Article): void {
    this.articleService.deleteArticle(article.id)
      .subscribe(data => {
        this.articles = this.articles.filter(u => u !== article);
      })
  };

  editArticle(article: Article): void {
    localStorage.removeItem("editArticleId");

    console.log(article);
    localStorage.setItem("editArticleId", article.id.toString());
    this.router.navigate(['/article/edit/' + article.id]);
  };

  addArticle(): void {
    this.router.navigate(['add-user']);
  };

}
