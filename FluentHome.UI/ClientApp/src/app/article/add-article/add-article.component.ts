
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AddArticle, Article } from '../../_models/article';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '../../_services';
import { ArticleService } from '../../_services/article.service';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { pipe } from 'rxjs';
import { ArticleCategoryService } from '../../_services/article-category.service';
import { ArticleCategory } from '../../_models/articlecategory';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.scss']
})
export class AddArticleComponent implements OnInit {

  addArticleForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private articleService: ArticleService,
    private articleCategoryService: ArticleCategoryService
  ) { }


  imgUrl: string = "/assets/images/default-thumb.jpg";
  fileToUpload: File = null;

  //bookImageUpload
  handleFileInput(file: FileList) {
    this.fileToUpload = file.item(0);

    //show preview
    var reader = new FileReader();
    reader.onload = (event: any) => {
      this.imgUrl = event.target.result;
    }
    reader.readAsDataURL(this.fileToUpload);
  }
  /********************************* */
  public articleCategoryId: string = '';
  public articleTitle: string = '';
  public articleDescription: string = '';
  public featuredImage: string = '';
  public isActive: boolean = false;
  articleCategories: ArticleCategory[] = [];
  public editorConfig: any;
  /********************************* */

  ngOnInit() {
    this.addArticleForm = this.formBuilder.group({
      // id: [],
      articleCategoryId: this.articleCategoryId,
      articleTitle: ['', Validators.required],
      articleDescription: this.articleDescription,
      featuredImage: this.featuredImage,
      isActive: this.isActive
    });

    this.articleCategoryService.getArticleCategories().subscribe((data: any) => {
      this.articleCategories = data;
    });

    this.editorConfig = {
      "editable": true,
      "spellcheck": true,
      "height": "auto",
      "minHeight": "0",
      "width": "auto",
      "minWidth": "0",
      "translate": "yes",
      "enableToolbar": true,
      "showToolbar": true,
      // "placeholder": "Enter text here...",
      // "imageEndPoint": "",
      // "toolbar": [
      //   ["bold", "italic", "underline", "strikeThrough"],
      //   ["justifyLeft", "justifyRight", "justifyFull", "outdent"]
      // ]
    };
  }

  // convenience getter for easy access to form fields
  get f() { return this.addArticleForm.controls; }

  article: Article;
  onArticleSubmit(formArticle) {

    this.submitted = true;

    // stop here if form is invalid
    if (this.addArticleForm.invalid) {
      return;
    }

    if (this.addArticleForm.valid) {

      this.loading = true;

      this.articleService.addArticle(formArticle)
        .subscribe(
          (data: any) => {
            //this.router.navigate([this.returnUrl]);
            this.router.navigate(['/article/list']);
            this.toastr.success('Article successfully added!');
          },
          error => {
            this.alertService.error(error);
            this.toastr.error('Unable to add article!');
            this.loading = false;
          });

    }

  }
}
