// import { NgModule } from '@angular/core';
// import { Routes, RouterModule } from '@angular/router';

// const routes: Routes = [];

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { AuthGuard } from './_guards';
import { ArticleCategoryListComponent } from './article-category/article-category-list/article-category-list.component';
import { ArticleCategoryComponent } from './article-category/article-category.component';
import { ArticleCategoryFormComponent } from './article-category/article-category-form/article-category-form.component';
import { ArticleListComponent } from './article/article-list/article-list.component';
import { AddArticleComponent } from './article/add-article/add-article.component';
import { UserRoleListComponent } from './user-role/user-role-list/user-role-list.component';
import { UserRoleFormComponent } from './user-role/user-role-form/user-role-form.component';
import { PermissionComponent } from './permission/permission.component';
import { ArticleComponent } from './article/article.component';
// import { UserTypeComponent } from './user-type/user-type.component';
// import { UserTypeListComponent } from './user-type/user-type-list/user-type-list.component';
import { EditArticleComponent } from './article/edit-article/edit-article.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { AddUserTypeComponent } from './test/add-user-type/add-user-type.component';
import { ListUserTypeComponent } from './test/list-user-type/list-user-type.component';
import { EditUserTypeComponent } from './test/edit-user-type/edit-user-type.component';
// import { AddUserTypeComponent } from './user-type/add-user-type/add-user-type.component';
// import { EditUserTypeComponent } from './user-type/edit-user-type/edit-user-type.component';
// import { ArticleListComponent } from './article/article-list/article-list.component';

const routes: Routes = [

  { path: "", redirectTo: '/fluent-admin', pathMatch: 'full' },
  { path: "fluent-admin", component: UserLoginComponent },
  { path: "dashboard", component: DashboardComponent, canActivate: [AuthGuard] },
  
  { path: 'add-user-type', component: AddUserTypeComponent, canActivate: [AuthGuard] },
  { path: 'edit-user-type', component: EditUserTypeComponent, canActivate: [AuthGuard] },
  { path: 'list-user-type', component: ListUserTypeComponent, canActivate: [AuthGuard] },

  {
    path: "articlecategory",
    component: ArticleCategoryComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "list", component: ArticleCategoryListComponent },
      { path: "add", component: ArticleCategoryFormComponent },
    ]
  },
  {
    path: "article",
    //component: ArticleComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "list", component: ArticleListComponent, canActivate: [AuthGuard] },
      { path: "add", component: AddArticleComponent, canActivate: [AuthGuard] },
      { path: "edit/:id", component: EditArticleComponent, canActivate: [AuthGuard] },
    ]
  },
  {
    path: "role",
    canActivate: [AuthGuard],
    children: [
      { path: "list", component: UserRoleListComponent },
      { path: "add", component: UserRoleFormComponent },
    ]
  },  
  {
    path: "permission", component: PermissionComponent, canActivate: [AuthGuard]
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }