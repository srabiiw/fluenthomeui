import { DataEntity } from "./data-entity";

export class ArticleCategory extends DataEntity {
    name: string;
    createdDate: Date;
    modifiedDate: Date;
    isActive: boolean;
}

export class AddArticleCategory {
    articleCategoryName: string;
    createdDate: Date;
    modifiedDate: Date;
    isActive: boolean;
}