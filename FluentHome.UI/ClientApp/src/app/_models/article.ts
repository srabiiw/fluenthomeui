import { DataEntity } from "./data-entity";

export class Article extends DataEntity {
    name: string;
    articleCategoryId: number;
    articleDescription: string;
    featuredImage: string;
    createdDate: Date;
    modifiedDate: Date;
    isActive: boolean;
}

export class AddArticle {
    articleTitle: string;
    articleCategoryId: number;
    featuredImage: string;
    articleDescription: string;
    isVerified: boolean;
    createdDate: Date;
    modifiedDate: Date;
    isActive: boolean;
}

export class ArticleList {
  id: number;
  articleTitle: string;
  articleCategoryName: string;
  featuredImage: string;
  isVerified: string;
  isActive: string;
}