export class UserType {
    id: number;
    userTypeName: string;
    createdDate: Date;
    modifiedDate: Date;
    isActive: boolean;
}