import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  year: Date;
  companyInfo: string;

  constructor() {
    this.companyInfo = 'Fluent Home';
    this.year = new Date();
  }

  ngOnInit() {
  }

}
