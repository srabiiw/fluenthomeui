import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserType } from '../_models';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserTypeService {

  constructor(private httpClient: HttpClient) { }

  public getUserTypes(): Observable<UserType[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    var userTypeList = this.httpClient.get<UserType[]>(`${environment.apiUrl}/user/getUserTypes`, httpOptions);
    return userTypeList;
  }

  getUserTypeById(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    return this.httpClient.get<UserType>(`${environment.apiUrl}/user/getUserTypeById/${id}`, httpOptions);
  }

  createUserType(userType: UserType) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    console.log(userType);
    
    return this.httpClient.put(`${environment.apiUrl}user/createUserType`, userType, httpOptions);
  }

  updateUserType(userType: UserType) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };
    
    return this.httpClient.put(`${environment.apiUrl}user/updateUserType`, userType, httpOptions);
  }

  deleteUserType(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    // return this.httpClient.delete(environment.apiUrl + '/' + id);
    return this.httpClient.put(`${environment.apiUrl}user/deleteUserType/${id}`, httpOptions);
  }

}
