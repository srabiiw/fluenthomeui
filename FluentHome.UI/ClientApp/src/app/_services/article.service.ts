import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Article, DataTablesResponse } from '../_models';

@Injectable({
  providedIn: 'root'
})
// export class ArticleService extends HttpGenericCrudService<Article> {
  export class ArticleService {

  // constructor(httpClient: HttpClient) {
  //   super(
  //     httpClient,
  //     environment.apiUrl,
  //     'article');
  // }

  constructor(
    protected httpClient: HttpClient
  ) { }

  public addArticle(entity: Article) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        //'Authorization': 'my-auth-token'
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    console.log(entity);

    return this.httpClient.post(`${environment.apiUrl}article/createArticle`, entity, httpOptions);
  }

  updateArticle(article: Article) {
    return this.httpClient.put(`${environment.apiUrl}article/udpateArticle` + article.id, article);
  }

  deleteArticle(id: number) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        //'Authorization': 'my-auth-token'
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    return this.httpClient.delete(`${environment.apiUrl}article/deleteArticle` + '/' + id, httpOptions);
  }

  public getArticles(): Observable<Article[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    var articles = this.httpClient.get<Article[]>(`${environment.apiUrl}article/getArticle`, httpOptions);
    return articles;
  }

  public getArticlesFromDataTable(dataTablesParameters) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    var articleList = this.httpClient.post<DataTablesResponse>(`${environment.apiUrl}article/getArticleList`, dataTablesParameters, httpOptions);
    
    //console.log(articleList);
    return articleList;
    // return this.httpClient.post<DataTablesResponse>(`${environment.apiUrl}article/getArticleList`, dataTablesParameters, { headers: this.httpHeaders }).pipe(catchError(this.errorHandler));
    // var articles = this.httpClient.post<DataTablesResponse>(`${environment.apiUrl}article/getArticleList`, httpOptions);
    // return articles;
  }

  public getArticleById(string: number): Observable<Article> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem('currentUser')
      })
    };

    var articleById = this.httpClient.get<Article>(`${environment.apiUrl}article/getArticleByID`, httpOptions);
    return articleById;
  }


  postFile(fileToUpload: File) {
    const endpoint = 'http://localhost:49760/api/UploadImage';
    const formData: FormData = new FormData();
    formData.append('Image', fileToUpload, fileToUpload.name);
    return this.httpClient.post(endpoint, formData, { headers: new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('userToken') }) });
  }
}