import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ArticleCategory } from '../_models';

@Injectable({
  providedIn: 'root'
})
// export class ArticleCategoryService extends HttpGenericCrudService<ArticleCategory> {
export class ArticleCategoryService {

  // constructor(httpClient: HttpClient) {
  //   super(
  //     httpClient,
  //     environment.apiUrl,
  //     'article');
  // }

  constructor(
    protected httpClient: HttpClient
    // private endPoint: string
  ) { }

  public addArticleCategory(entity: ArticleCategory) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        //'Authorization': 'my-auth-token'
      })
    };

    return this.httpClient.post(`${environment.apiUrl}/article/createArticleCategory`, entity, httpOptions);
  }

  public getArticleCategories(): Observable<ArticleCategory[]> {
    // return this.http.get<Employee[]>('http://localhost:54470/api/employees');
    var articleCategories = this.httpClient.get<ArticleCategory[]>(`${environment.apiUrl}/article/getArticleCategories`);
    return articleCategories;
  }

}