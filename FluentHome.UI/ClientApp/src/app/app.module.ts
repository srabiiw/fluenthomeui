import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG } from '@ng-select/ng-select';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CKEditorModule } from 'ngx-ckeditor';
import { DataTablesModule } from 'angular-datatables';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlertComponent } from './_directives/alert/alert.component';
import { ArticleComponent } from './article/article.component';
import { AddArticleComponent } from './article/add-article/add-article.component';
import { ArticleListComponent } from './article/article-list/article-list.component';
import { EditArticleComponent } from './article/edit-article/edit-article.component';
import { ArticleCategoryComponent } from './article-category/article-category.component';
import { ArticleCategoryFormComponent } from './article-category/article-category-form/article-category-form.component';
import { ArticleCategoryListComponent } from './article-category/article-category-list/article-category-list.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PageNotFoundComponent } from './error/page-not-found/page-not-found.component';
import { PermissionComponent } from './permission/permission.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { UserRoleComponent } from './user-role/user-role.component';
import { UserRoleFormComponent } from './user-role/user-role-form/user-role-form.component';
import { UserRoleListComponent } from './user-role/user-role-list/user-role-list.component';
import { UserTypeComponent } from './user-type/user-type.component';
import { HeaderComponent } from './widgets/header/header.component';
import { FooterComponent } from './widgets/footer/footer.component';

import { AuthGuard } from './_guards';
import { AlertService, AuthenticationService, UserService, ArticleCategoryService, ArticleService } from './_services';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AddUserTypeComponent } from './test/add-user-type/add-user-type.component';
import { EditUserTypeComponent } from './test/edit-user-type/edit-user-type.component';
import { ListUserTypeComponent } from './test/list-user-type/list-user-type.component';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    ArticleComponent,
    AddArticleComponent,
    ArticleListComponent,
    EditArticleComponent,
    ArticleCategoryComponent,
    ArticleCategoryFormComponent,
    ArticleCategoryListComponent,
    DashboardComponent,
    PageNotFoundComponent,
    PermissionComponent,
    UserLoginComponent,
    UserRoleComponent,
    UserRoleFormComponent,
    UserRoleListComponent,
    UserTypeComponent,
    HeaderComponent,
    FooterComponent,
    AddUserTypeComponent,
    EditUserTypeComponent,
    ListUserTypeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    // NgxPaginationModule,
    CKEditorModule,
    NgSelectModule,
    DataTablesModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    ArticleCategoryService,
    ArticleService,
    // UserTypeService,
    // FakeBackendInterceptor,
    // {
    //   provide: NG_SELECT_DEFAULT_CONFIG,
    //   useValue: {
    //     placeholder: 'Select item',
    //     notFoundText: 'Items not found',
    //     addTagText: 'Add item',
    //     typeToSearchText: 'Type to search',
    //     loadingText: 'Loading...',
    //     clearAllText: 'Clear all'
    //   }
    // },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    HttpClient
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
