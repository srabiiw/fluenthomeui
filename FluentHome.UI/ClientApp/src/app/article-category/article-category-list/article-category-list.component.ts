import { Component, OnInit, HostListener, ViewChildren, QueryList, ElementRef, Output, EventEmitter } from '@angular/core';
import { Http } from '@angular/http';
import { AlertService } from '../../_services';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleCategoryService } from '../../_services/article-category.service';

@Component({
  selector: 'app-article-category-list',
  templateUrl: './article-category-list.component.html',
  styleUrls: ['./article-category-list.component.scss']
})
export class ArticleCategoryListComponent implements OnInit {

  public articleCategories: any;

  @Output() articleClick = new EventEmitter();

  constructor(
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private articleCategoryService: ArticleCategoryService
  ) { }

  ngOnInit() {
    this.articleCategoryService.getArticleCategories().subscribe((data: any) => {
      this.articleCategories = data;
    });
  }

  artClick(value){
    this.articleClick.emit(value);
  }


}
