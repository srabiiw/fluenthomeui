import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService, ArticleCategoryService } from '../../_services';
import { ToastrService } from 'ngx-toastr';
import { first } from 'rxjs/operators';
import { pipe } from 'rxjs';
import { ArticleCategory } from '../../_models';

@Component({
  selector: 'app-article-category-form',
  templateUrl: './article-category-form.component.html',
  styleUrls: ['./article-category-form.component.scss']
})
export class ArticleCategoryFormComponent implements OnInit {

  articleCategoryForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService,
    private articleCategoryService: ArticleCategoryService
  ) { }

  ngOnInit() {
    this.articleCategoryForm = this.formBuilder.group({
      articleCategoryName: ['', Validators.required],
      isActive: ['']
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.articleCategoryForm.controls; }

  article: ArticleCategory;
  onArticleCategorySubmit(formArticle) {

    // @Output() myEvent = new EventEmitter();


    this.submitted = true;

    // stop here if form is invalid
    if (this.articleCategoryForm.invalid) {
      return;
    }

    if (this.articleCategoryForm.valid) {

      // var articleCategory: ArticleCategory = {
      //   name: this.articleCategoryForm.value.articleCategoryName,
      //   createdDate: new Date(),
      //   modifiedDate: null,
      //   isActive: this.articleCategoryForm.value.isActive == "" ? false : true
      // };

      // this.articleCategoryService.AddEntity(formArticle)
      //   .subscribe((data: any) => {
      //     console.log(data);
      //     if (data.Id != null) {
      //       // this.resetForm(form);
      //       // this.programs.push(data);          
      //       // this.toastr.success('Program successfully added!');
      //     }
      //     else {
      //       // this.toastr.error('Program not added');
      //     }
      //   });


      this.loading = true;
      // this.articleCategoryService.addArticleCategory(formArticle)
      //   .subscribe((data: any) => {
      //     if (data.Id != null) {
      //       // this.resetForm(form);
      //       // this.programs.push(data);          
      //       // this.toastr.success('Program successfully added!');
      //     }
      //     else {
      //       // this.toastr.error('Program not added');
      //     }
      //   });

      this.articleCategoryService.addArticleCategory(formArticle)
        .subscribe(
          (data: any) => {
            //this.router.navigate([this.returnUrl]);
            this.router.navigate(['/articlecategory/list']);
            this.toastr.success('Article category successfully added!');
          },
          error => {
            this.alertService.error(error);
            this.toastr.error('Unable to add article category!');
            this.loading = false;
          });

      //this.articleCategoryService.addArticleCategory(addArticleCategory);
      // .pipe(first())
      // .subscribe(
      //   (data: any) => {
      //     //this.router.navigate([this.returnUrl]);
      //     this.router.navigate(['/articlecategory/list']);
      //   },
      //   error => {
      //     this.alertService.error(error);
      //     this.loading = false;
      //   });

    }
  }

}
