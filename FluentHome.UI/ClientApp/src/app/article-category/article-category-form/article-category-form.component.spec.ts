import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleCategoryFormComponent } from './article-category-form.component';

describe('ArticleCategoryFormComponent', () => {
  let component: ArticleCategoryFormComponent;
  let fixture: ComponentFixture<ArticleCategoryFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleCategoryFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleCategoryFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
